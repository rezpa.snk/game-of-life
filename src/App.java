public class App {
    public static void main(String[] args) {
        int gridSize = 50;
        int cellSize = 10; //size on frame
        int frameSize = gridSize * cellSize;

        Field field = new Field(gridSize, cellSize);
        View view = new View(field, frameSize, 400);

        // "plus" pattern
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(10, 7)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(10, 8)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(10, 6)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(9, 7)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(11, 7)));

        // "gliding" pattern
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(15,10)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(16,11)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(17,10)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(17,11)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(17,9)));
//
//        // "beacon" pattern
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(25,25)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(25,26)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(26,25)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(26,26)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(27,27)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(27,28)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(28,27)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(28,28)));
//
//        // pentadecathlon pattern
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(5,20)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(6,20)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(7,19)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(7,21)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(8,20)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(9,20)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(10,20)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(11,20)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(12,19)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(12,21)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(13,20)));
        field.setCellAtPosition(new Cell(CellState.alive, field, new Location(14,20)));

    }
}
