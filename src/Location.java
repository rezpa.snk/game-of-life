import java.util.ArrayList;

public class Location {
    private int x;
    private int y;

    public Location(int x, int y){
        this.x = x;
        this.y = y;
    }

    public ArrayList<Location> getNeighborLocation(){
        ArrayList<Location> neighborLocation = new ArrayList<>();
        for(int i=-1;i<2;i++){
            for(int j=-1;j<2;j++){
                if(i==0 && j==0){
                    continue;
                }
                if(((x+i) >= 0 && (x+i) < Field.gridSize) && ((y+j) >= 0 && (y+j) < Field.gridSize)){
                    neighborLocation.add(new Location((x+i), (y+j)));
                }
            }
        }
        return neighborLocation;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
