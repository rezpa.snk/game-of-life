import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class Cell {
    private CellState state;
    private Location location;
    private ArrayList<Cell> neighbors;
    private Field field;

    public Cell(CellState state, Field field, Location location){
        this.state = state;
        this.location = location;
        this.field = field;
        neighbors = new ArrayList<>();
    }

    public Location getLocation(){
        return location;
    }

    public CellState getState(){
        return state;
    }

    public void inspect(Rule rule, ArrayList<Cell> potDeadCell, ArrayList<Cell> potBornCell){
        if(state == CellState.alive){
            if(rule.checkRuleforDie(countAliveNeighbor())){
                potDeadCell.add(this);
            }
        }else{
            if(rule.checkRuleforBorn(countAliveNeighbor())){
                potBornCell.add(this);
            }
        }
    }

    public void changeState(){
        if(state == CellState.dead){
            state = CellState.alive;
        }else{
            state = CellState.dead;
        }
    }

    //this method will only be called once
    public ArrayList<Cell> getNeighbor(){
        if(neighbors.size() == 0){
            for (Location loc: location.getNeighborLocation()){
                neighbors.add(field.getCellAtPosition(loc));
            }
        }

        return neighbors;
    }

    public int countAliveNeighbor(){
        int count = 0;
        if(neighbors.size() == 0) {
            getNeighbor();
        }

        for (Cell cell : neighbors) {
            if (cell.getState() == CellState.alive) {
                count++;
            }
        }

        return count;
    }

    public void paint(Graphics g){
        g.setColor(Color.LIGHT_GRAY);

        if(state == CellState.alive){
            g.setColor(Color.DARK_GRAY);
        }

        g.fillRect(location.getY() * Field.cellSize, location.getX() * Field.cellSize, Field.cellSize, Field.cellSize);
    }
}
