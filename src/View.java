import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class View extends JPanel implements ActionListener {

    final int width;
    final int height;
    final int delay;
    Field field;

    public View(Field field, int frameSize, int delay){
        this.field = field;
        this.delay = delay;

        width = frameSize+15;
        height = frameSize+35;

        JFrame frame = new JFrame("Game of Life v3");
        frame.setSize(width, height);
        frame.add(this);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setResizable(false);

        Timer timer = new Timer(delay, this);
        timer.restart();

    }

    public void paint(Graphics g){
        super.paintComponent(g);
        field.populate(g);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        repaint();
    }
}
