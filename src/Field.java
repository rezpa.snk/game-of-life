import java.awt.*;
import java.security.SecureRandom;
import java.util.ArrayList;

public class Field {
    static int gridSize;
    static int cellSize;

    private Rule rule;
    private Cell collectionOfCell[][];
    private ArrayList<Cell> potentialBornCell = new ArrayList<>();
    private ArrayList<Cell> potentialDeadCell = new ArrayList<>();

    public Field(int gridSize, int cellSize){
        this.gridSize = gridSize;
        this.cellSize = cellSize;

        rule = new Rule();
        collectionOfCell = new Cell[gridSize][gridSize];

        createInitialCollection();

    }

    public Cell getCellAtPosition(Location location){
        return collectionOfCell[location.getX()][location.getY()];
    }

    public void populate(Graphics g){
        for(Cell[] row: collectionOfCell){
            for(Cell cell: row){
                if(cell != null){
                    cell.paint(g);
                    cell.inspect(rule, potentialDeadCell, potentialBornCell);
                }
            }
        }

        populateNewCell(potentialBornCell);
        populateNewCell(potentialDeadCell);

    }

    private void populateNewCell(ArrayList<Cell> newCell){
        if(newCell.size() > 0){
            for (Cell cell: newCell){
                cell.changeState();
            }
            newCell.clear();
        }
    }


    public void createInitialCollection(){
        for(int i=0;i<gridSize;i++){
            for(int j=0;j<gridSize;j++){
                collectionOfCell[i][j] = new Cell(CellState.dead, this, new Location(i, j));
//                collectionOfCell[i][j] = new Cell(getRandomState(), this, new Location(i, j));
            }
        }
    }

    public void setCellAtPosition(Cell cell){
        if(cell != null) { collectionOfCell[cell.getLocation().getX()][cell.getLocation().getY()] = cell;}
    }

    public CellState getRandomState(){
        SecureRandom random = new SecureRandom();
        return random.nextBoolean() ? CellState.alive : CellState.dead;
    }
}
